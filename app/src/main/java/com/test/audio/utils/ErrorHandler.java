package com.test.audio.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ErrorHandler {

    public static void handleError(Context context, Exception e) {
        Log.d("TabletopAudio", "Error occured", e);
        displayError(context, e.getLocalizedMessage());
    }

    public static void displayError(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
